<?php 

namespace Acme\AssetPath;

use Illuminate\Support\Collection;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\Storage;

class AssetPath extends UrlGenerator
{	
	/**
     * Generate a URL to an application asset.
     *
     * @param  string  $path
     * @param  bool|null  $secure
     * @return string
     */
    public function asset($path, $secure = null)
    {
        if ($this->isValidUrl($path)) {
            return $path;
        }

        $s3_active = strtolower(getenv('S3_ACTIVE'));
        // Once we get the root URL, we will check to see if it contains an index.php
        // file in the paths. If it does, we will remove it since it is not needed
        // for asset paths, but only for routes to endpoints in the application.
        if ($s3_active == "true") {
            if ($path) {
                return $this->removeIndex(Storage::disk('s3')->url(trim($path, '/')));
            } else {
                return $this->removeIndex('https://' . getenv('S3_BUCKET') . '.s3.' . getenv('S3_REGION') . '.amazonaws.com/');
            }
        } else {
            $root = $this->getRootUrl($this->getScheme($secure));
            return $this->removeIndex($root).'/'.trim($path, '/');
        }
    }
}